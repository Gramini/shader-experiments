﻿using System;
using UnityEngine;

namespace GyroidControll
{
    public class Steps : MonoBehaviour
    {
        [SerializeField]
        private float speed = 50f;

        private float steps;
        private Material mat;

        private static readonly int MaxSteps = Shader.PropertyToID("_MaxSteps");

        private void Start()
        {
            mat = GetComponent<MeshRenderer>().material;
            steps = mat.GetInt(MaxSteps);
        }

        void Update()
        {
            bool update = false;

            if (Input.GetKey(KeyCode.UpArrow))
            {
                steps = Mathf.Max(1, steps + Time.deltaTime * speed);
                update = true;
            }

            if (Input.GetKey(KeyCode.DownArrow))
            {
                steps = Mathf.Max(1, steps - Time.deltaTime * speed);
                update = true;
            }

            if (update)
            {
                mat.SetInt(MaxSteps, (int)steps);
            }
        }
    }
}
