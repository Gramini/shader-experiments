﻿using System;
using UnityEngine;

namespace Highlight
{
    public class HighlightPoint : MonoBehaviour
    {
        [SerializeField]
        private HighlightManager highlightManager;

        [SerializeField]
        private float radius = 1f;

        [SerializeField]
        private float softness = .1f;

        private Vector3 lastPosition;
        private IHighlight highlight;

        public void SetRadius(float newRadius)
        {
            highlight.SetRadius(newRadius);
            highlightManager.UpdateHighlight(highlight);
        }

        public void SetSoftness(float newSoftness)
        {
            highlight.SetSoftness(newSoftness);
            highlightManager.UpdateHighlight(highlight);
        }

        private void OnValidate()
        {
            if (!highlightManager || highlight == null)
            {
                return;
            }

            highlight.SetRadius(radius);
            highlight.SetSoftness(softness);

            highlightManager.UpdateHighlight(highlight);
        }

        private void OnEnable()
        {
            if (!highlightManager)
            {
                Debug.LogError($"HighlightPoint without access to a HighlightManager ({name})");
                return;
            }

            highlight = highlightManager.RequestHighlight();

            if (highlight == null)
            {
                return;
            }

            highlight.SetPosition(transform.position);
            highlight.SetRadius(radius);
            highlight.SetSoftness(softness);

            highlightManager.UpdateHighlight(highlight);
        }

        private void OnDisable()
        {
            if (!highlightManager)
            {
                return;
            }

            highlightManager.ReleaseHighlight(highlight);
        }

        void Update()
        {
            if (!highlightManager || highlight == null)
            {
                return;
            }

            Vector3 currentPosition = transform.position;
            if (currentPosition != lastPosition)
            {
                highlight.SetPosition(currentPosition);
                highlightManager.UpdateHighlight(highlight);
            }

            lastPosition = currentPosition;
        }
    }
}
