using UnityEngine;

namespace Highlight
{
    public interface IHighlight
    {
        int GetId();
        Vector3 GetPosition();
        float GetRadius();
        float GetSoftness();

        void SetPosition(Vector3 newPosition);
        void SetRadius(float newRadius);
        void SetSoftness(float newSoftness);
    }
}
