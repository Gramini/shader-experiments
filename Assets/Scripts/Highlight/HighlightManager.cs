﻿using Extensions;
using JetBrains.Annotations;
using UnityEngine;

namespace Highlight
{
    public class HighlightManager : MonoBehaviour
    {
        private const int HighlightCount = 10;

        private int highlightCount = 0;
        private readonly bool[] registeredHighlights = new bool[HighlightCount];
        private readonly Vector4[] positions         = new Vector4[HighlightCount];
        private readonly float[] radii               = new float[HighlightCount];
        private readonly float[] softnesses          = new float[HighlightCount];

        private bool doUpdate;

        private static readonly int FaHighlightCount      = Shader.PropertyToID("_FA_Highlight_Count");
        private static readonly int FaHighlightPositions  = Shader.PropertyToID("_FA_Highlight_Positions");
        private static readonly int FaHighlightRadii      = Shader.PropertyToID("_FA_Highlight_Radii");
        private static readonly int FaHighlightSoftnesses = Shader.PropertyToID("_FA_Highlight_Softnesses");

//        private void Start()
//        {
//            for (int i = 0; i < HighlightCount; i++)
//            {
//                positions[i]  = new Vector4();
//                radii[i]      = 0;
//                softnesses[i] = 0;
//            }
//
//            DoUpdate();
//        }

        [CanBeNull]
        public IHighlight RequestHighlight()
        {
            for (int i = 0; i < registeredHighlights.Length; i++)
            {
                // ReSharper disable once InvertIf
                if (!registeredHighlights[i])
                {
                    highlightCount++;
                    registeredHighlights[i] = true;

                    return new Highlight(i);
                }
            }

            return null;
        }

        public void ReleaseHighlight(IHighlight highlight)
        {
            int id = highlight.GetId();

            registeredHighlights[id] = false;
            UpdateHighlight(new Highlight(id));

            highlightCount--;
        }

        public void UpdateHighlight(IHighlight highlight)
        {
            int id = highlight.GetId();

            positions[id]  = highlight.GetPosition().ToVector4();
            radii[id]      = highlight.GetRadius();
            softnesses[id] = highlight.GetSoftness();

            doUpdate = true;
        }

        private void LateUpdate() => DoUpdate();

        private void DoUpdate()
        {
            if (!doUpdate)
            {
                return;
            }

            Shader.SetGlobalInt(FaHighlightCount, highlightCount);
            Shader.SetGlobalVectorArray(FaHighlightPositions, positions);
            Shader.SetGlobalFloatArray(FaHighlightRadii, radii);
            Shader.SetGlobalFloatArray(FaHighlightSoftnesses, softnesses);

            doUpdate = false;
        }

        private class Highlight : IHighlight
        {
            private Vector3 position;
            private float radius;
            private float softness;

            private int id;

            public Highlight(int id)
            {
                this.id = id;
            }

            public int GetId()
            {
                return id;
            }

            public Vector3 GetPosition()
            {
                return position;
            }

            public float GetRadius()
            {
                return radius;
            }

            public float GetSoftness()
            {
                return softness;
            }

            public void SetPosition(Vector3 newPosition)
            {
                position = newPosition;
            }

            public void SetRadius(float newRadius)
            {
                radius = newRadius;
            }

            public void SetSoftness(float newSoftness)
            {
                softness = newSoftness;
            }
        }
    }
}
