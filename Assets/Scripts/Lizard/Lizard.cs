﻿using System.Collections.Generic;
using UnityEngine;

namespace Lizard
{
    public class Lizard : MonoBehaviour
    {
        [SerializeField]
        float bodyPartRadius = .25f;

        [SerializeField]
        [Range(2, 100)]
        int bodyPartCount = 7;

        [SerializeField]
        [Range(0f, .9f)]
        float overlapPercentage = .3f;

        List<BodyPart> bodyParts = new List<BodyPart>();

        public BodyPart Head => bodyParts[0];
        
        void Update()
        {
            FormBody();

            BodyPart prevBodyPart = null;
            foreach (BodyPart bodyPart in bodyParts)
            {
                // ReSharper disable once Unity.PerformanceCriticalCodeNullComparison
                if (prevBodyPart == null)
                {
                    prevBodyPart = bodyPart;
                    continue;
                }

                Vector3 aToB      = prevBodyPart.transform.position - bodyPart.transform.position;
                float distance    = aToB.magnitude;
                Vector3 direction = aToB.normalized;
                float toMove      = distance - prevBodyPart.Radius - bodyPart.Radius + bodyPart.Radius * overlapPercentage * 2;
                
                bodyPart.transform.Translate(direction * toMove);
                
                prevBodyPart = bodyPart;
            }
        }

        void Awake()
        {
            FormBody();
            BodyPart head = Head;
            GameObject headGo = head.gameObject;
            
            SphereCollider addedCollider = headGo.AddComponent<SphereCollider>();
            Rigidbody addedRb = headGo.AddComponent<Rigidbody>();

            addedCollider.radius = head.Radius;
        }
//        void OnEnable() => BuildBody();

//        void OnValidate()
//        {
//            bodyPartRadius = Mathf.Max(bodyPartRadius, .05f);
//            bodyPartCount  = Mathf.Max(bodyPartCount, 2);
//            
//            if (!Application.isPlaying)
//            {
//                return;
//            }
//            
//            foreach (Transform child in transform) {
//                Destroy(child.gameObject);
//            }
//        }

        void FormBody()
        {
            int toAdd = bodyPartCount - bodyParts.Count;
            
            if (toAdd == 0)
            {
                return;
            }
            
            if (toAdd > 0)
            {
                AddBodyParts(toAdd);
                return;
            }
            
            RemoveBodyParts(-toAdd);
        }
        
        void AddBodyParts(int amount)
        {
            int baseBodyIndex = bodyParts.Count;

            for (int i = 0; i < amount; i++)
            {
                int bodyIndex = baseBodyIndex + i;
                
                GameObject bodyPart   = new GameObject($"body_{bodyIndex}");
                BodyPart realBodyPart = bodyPart.AddComponent<BodyPart>();
                realBodyPart.Radius   = bodyPartRadius;
                
                Transform legTransform = bodyPart.transform;
                legTransform.parent    = transform;

                if (bodyParts.Count < 2)
                {
                    legTransform.localPosition = -transform.forward * bodyParts.Count;
                    
                    bodyParts.Add(realBodyPart);
                    continue;
                }
                
                BodyPart last       = bodyParts[bodyParts.Count - 1];
                BodyPart secondLast = bodyParts[bodyParts.Count - 2];

                Vector3 lastPosition     = last.transform.position;
                Vector3 secondLastToLast = lastPosition - secondLast.transform.position;
                float distance           = secondLastToLast.magnitude;
                Vector3 direction        = secondLastToLast.normalized;

                legTransform.position = lastPosition + direction * distance;

                bodyParts.Add(realBodyPart);
            }
        }

        void RemoveBodyParts(int amount)
        {
            for (int i = bodyParts.Count - amount; i < bodyParts.Count; i++)
            {
                BodyPart bodyPart = bodyParts[i];
                bodyParts.Remove(bodyPart);

                Destroy(bodyPart.gameObject);
            }
        }

        void OnDrawGizmos()
        {
            Transform trans  = transform;
            Vector3 position = trans.position;

            Gizmos.DrawLine(position, position + -2 * (bodyPartCount - 1) * bodyPartRadius * trans.forward);
            
            
//            Gizmos.color = Color.yellow;
//            BodyPart last       = bodyParts[bodyParts.Count - 1];
//            BodyPart secondLast = bodyParts[bodyParts.Count - 2];
//
//            Vector3 lastPosition     = last.transform.position;
//            Vector3 secondLastToLast = lastPosition - secondLast.transform.position;
//            float distance           = secondLastToLast.magnitude;
//            Vector3 direction        = secondLastToLast.normalized;
//            Gizmos.DrawLine(lastPosition, lastPosition + direction * distance);
        }
    }
}
