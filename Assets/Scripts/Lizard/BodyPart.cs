﻿using System;
using UnityEngine;

namespace Lizard
{
    public class BodyPart : MonoBehaviour
    {
        [SerializeField]
        float radius = .25f;

//        public void SetRadius(float newRadius) => radius = newRadius;

        public float Radius
        {
            get => radius;
            set => radius = value;
        }

        void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawSphere(transform.position, radius);
        }
    }
}
