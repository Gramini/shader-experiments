using UnityEngine;

namespace Extensions
{
    public static class Vector3Extensions
    {
        public static Vector4 ToVector4(this Vector3 vector3, float w = 0f)
        {
            return new Vector4(
                vector3.x,
                vector3.y,
                vector3.z,
                w
            );
        }
    }
}
