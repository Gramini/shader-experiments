﻿Shader "Debug/Normals" {
    SubShader {
        Pass {
            CGPROGRAM
// Upgrade NOTE: excluded shader from DX11; has structs without semantics (struct v2f members normal)
// #pragma exclude_renderers d3d11
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
    
            // vertex input: position, normal
            struct appdata {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
    
            struct v2f {
                float4 pos : SV_POSITION;
                float3 normal : TEXCOORD1;
            };
            
            v2f vert (appdata v) {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex );
                o.normal = v.normal;
                return o;
            }
            
            fixed4 frag (v2f i) : SV_Target
            {
                float3 normal = i.normal;// + 1 * .5;// * .5 + .5;
                float len = pow(length(normal), 500);
                return fixed4(len, len, len, 0);
            }
            ENDCG
        }
    }
}
