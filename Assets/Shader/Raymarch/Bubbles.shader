﻿Shader "Raymarch/Bubbles"
{
    Properties
    {
        [Header(Visuals)]
        [Space]
        _BaseColor("Base Color", Color) = (0.8, 0.8, 0.8, 1)
        _AmbientColor("Ambient Color", Color) = (0.16, 0.16, 0.16, 1)

        [PowerSlider(10.)]
        _Gloss("Gloss", Range(.1, 100.)) = 15.

        [PowerSlider(.15)]
        _SpecularIntensity("Specular Intensity", Range(.1, 1)) = .98

        [PowerSlider(5)]
        _NormalSmoothing("Normal Smoothing", Range(.000005, .1)) = .005

        [Header(Behavior)]
        [Space]
        _MovementMultiplier("Movement Multiplier", Vector) = (1, 1, 1)
        _Seed("Start Time (Seed)", Float) = 100

        [Header(Performance)]
        [Space]
        _MaxSteps ("Max Steps", int) = 100
        _SurfaceDistance ("Surface Distance", float) = .01
        _MaxDistance ("Max Distance", float) = 100

        [Header(Debug)]
        [Space]
        [MaterialToggle]
        _DrawSteps ("Draw Steps", float) = 0
        [HDR]
        _DrawStepsColor ("Draw Step Color", Color) = (1., 0, 0)
    }

    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityLightingCommon.cginc"

            #include "inc/RaymarchUtils.cginc"
            #include "inc/DistFunctions.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float3 rayOrigin : TEXCOORD1;
                float3 hitPosition : TEXCOORD2;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;

                // Object Space
                o.rayOrigin = mul(unity_WorldToObject, float4(_WorldSpaceCameraPos, 1));
                o.hitPosition = v.vertex;

                // World Space
                // o.rayOrigin = _WorldSpaceCameraPos;
                // o.hitPosition = mul(unity_ObjectToWorld, v.vertex);
                return o;
            }

            struct RaymarchResult {
                float distance;
                float steps;
            };

            // Visuals
            float4 _BaseColor;
            float4 _AmbientColor;
            float _Gloss;
            float _SpecularIntensity;
            float _NormalSmoothing;

            // Performance
            int _MaxSteps;
            float _SurfaceDistance;
            float _MaxDistance;

            // Behavior
            float4 _MovementMultiplier;
            float _Seed;

            // Debug
            float _DrawSteps;
            float3 _DrawStepsColor;

            float Bubble(float3 position, float radius, float xSpeed = 1., float ySpeed = 1., float zSpeed = 1.) {
                float maxOffset = .5 - radius - .1;
                float speed = _MovementMultiplier.w;

                float3 pos = float3(
                    sin((_Time.y * xSpeed * speed * _MovementMultiplier.x) + (_Seed * xSpeed)) * maxOffset,
                    sin((_Time.y * ySpeed * speed * _MovementMultiplier.y) + (_Seed * ySpeed)) * maxOffset,
                    sin((_Time.y * zSpeed * speed * _MovementMultiplier.z) + (_Seed * zSpeed)) * maxOffset
                );

                return DistSphere(position - pos, radius);
            }

            float GetDist(float3 position) {
                //float s1 = DistSphere(position - float3(.1, .2, .1), .3);
                //float3 s3Pos = float3(
                //    sin(_Time.x * .7) * .275,
                //    _SinTime.w * -.275,
                //    0.
                //);
                //float s2 = DistSphere(position - s3Pos, .2);

                float s1 = Bubble(position, .21, 1.234, 1.645, .923);
                float s2 = Bubble(position, .1531, 1.12, 1.546, .81);
                float s3 = Bubble(position, .175, -2.12, .546, -1.31);
                float s4 = Bubble(position, .132, 1.131, -.312, -1.1234);
                float s5 = Bubble(position, .0843, .982, .27836, 3.1236);
                float s6 = Bubble(position, .093, .34582, .59786234, 1.53986);

                float dist = smin(s1, s2, .2);
                dist = smin(dist, s3, .2);
                dist = smin(dist, s4, .2);
                dist = smin(dist, s5, .2);
                dist = smin(dist, s6, .2);

                return dist;
            }

            RaymarchResult Raymarch(float3 rayOrigin, float3 rayDirection) {
                float distanceFromOrigin  = 0;
                float distanceFromSurface = 0;

                RaymarchResult result;

                for (int i = 0; i < _MaxSteps; i++) {
                    float3 position = rayOrigin + distanceFromOrigin * rayDirection;
                    distanceFromSurface = GetDist(position);
                    distanceFromOrigin += distanceFromSurface;

                    if (distanceFromSurface < _SurfaceDistance || distanceFromOrigin > _MaxDistance) {
                        result.steps = i;
                        break;
                    }
                }

                result.distance = distanceFromOrigin;

                return result;
            }

            float3 GetNormal(float3 position) {
                float2 epsilon = float2(_NormalSmoothing, 0);

                float3 normal = GetDist(position) -float3(
                    GetDist(position - epsilon.xyy),
                    GetDist(position - epsilon.yxy),
                    GetDist(position - epsilon.yyx)
                );

                return normalize(normal);
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float2 uv = i.uv - .5;

                float3 rayOrigin = i.rayOrigin; // float3(0, 0, -3);
                float3 rayDirection = normalize(i.hitPosition - rayOrigin); //normalize(float3(uv.x, uv.y, 1));

                RaymarchResult result = Raymarch(rayOrigin, rayDirection);

                if (_DrawSteps > 0) {
                    return fixed4(
                        (_DrawStepsColor * (result.steps / _MaxSteps)),
                        0
                    );
                }

                if (result.distance > _MaxDistance) {
                    discard;
                }

                float3 position = rayOrigin + rayDirection * result.distance;
                float3 normal = GetNormal(position);

                fixed3 col = AddLighting(position, normal, _BaseColor, _AmbientColor, _Gloss, _SpecularIntensity);

                return fixed4(col, 0);
            }
            ENDCG
        }
    }
}
