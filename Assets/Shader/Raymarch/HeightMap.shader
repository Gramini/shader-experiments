﻿Shader "Raymarch/HeightMap"
{
    Properties
    {
        [Header(Visuals)]
        [Space]
        _MainTex ("Main Texture", 2D) = "white" {}
        _HeightMap ("Height Map", 2D) = "white" {}

        _Height("Height", Range(-1., 1.)) = .1

        _BaseColor("Base Color", Color) = (0.8, 0.8, 0.8, 1)
        _AmbientColor("Ambient Color", Color) = (0.16, 0.16, 0.16, 1)

        [PowerSlider(10.)]
        _Gloss("Gloss", Range(.1, 100.)) = 15.

        [PowerSlider(.15)]
        _SpecularIntensity("Specular Intensity", Range(.1, 1)) = .98

        [PowerSlider(5)]
        _NormalSmoothing("Normal Smoothing", Range(.000005, .1)) = .005

        [MaterialToggle]
        _WorldSpace ("Use World Space", float) = 0

        [Header(Performance)]
        [Space]
        _MaxSteps ("Max Steps", int) = 100
        _SurfaceDistance ("Surface Distance", float) = .01
        _MaxDistance ("Max Distance", float) = 100

        [Header(Debug)]
        [Space]
        [MaterialToggle]
        _DrawSteps ("Draw Steps", float) = 0
        [HDR]
        _DrawStepsColor ("Draw Step Color", Color) = (1., 0, 0)
    }

    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityLightingCommon.cginc"

            #include "inc/RaymarchUtils.cginc"
            #include "inc/DistFunctions.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float3 rayOrigin : TEXCOORD1;
                float3 hitPosition : TEXCOORD2;
            };

            // Visuals
            sampler2D _HeightMap;
            float4 _HeightMap_ST;

            sampler2D _MainTex;
            float4 _MainTex_ST;

            float _Height;
            float4 _BaseColor;
            float4 _AmbientColor;
            float _Gloss;
            float _SpecularIntensity;
            float _NormalSmoothing;
            float _WorldSpace;

            // Performance
            int _MaxSteps;
            float _SurfaceDistance;
            float _MaxDistance;

            // Debug
            float _DrawSteps;
            float3 _DrawStepsColor;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;

                if (_WorldSpace <= 0) {
                    // Object Space
                    o.rayOrigin = mul(unity_WorldToObject, float4(_WorldSpaceCameraPos, 1));
                    o.hitPosition = v.vertex;

                    return o;
                }

                // World Space
                o.rayOrigin = _WorldSpaceCameraPos;
                o.hitPosition = mul(unity_ObjectToWorld, v.vertex);
                return o;
            }

            struct RaymarchResult {
                float distance;
                float steps;
            };

            float DistHeightMap(float3 position) {
                return position.y - tex2D(
                    _HeightMap,
                    (position.zx * _HeightMap_ST.xy) + _HeightMap_ST.zw
                ).r * _Height;
            }

            float GetDist(float3 position) {
                //return 1.;
                //return DistSphere(position, .1);
                return DistHeightMap(position);
            }

            RaymarchResult Raymarch(float3 rayOrigin, float3 rayDirection) {
                float distanceFromOrigin  = 0;
                float distanceFromSurface = 0;

                RaymarchResult result;
                result.steps = 0;

                [loop]
                for (int i = 0; i < _MaxSteps; i++) {
                    result.steps += 1;

                    float3 position = rayOrigin + distanceFromOrigin * rayDirection;
                    distanceFromSurface = GetDist(position);
                    distanceFromOrigin += distanceFromSurface * .5;

                    if (distanceFromSurface < _SurfaceDistance || distanceFromOrigin > _MaxDistance) {
                        break;
                    }
                }

                result.distance = distanceFromOrigin;

                return result;
            }

            float3 GetNormal(float3 position) {
                float2 epsilon = float2(_NormalSmoothing, 0);

                float3 normal = GetDist(position) -float3(
                    GetDist(position - epsilon.xyy),
                    GetDist(position - epsilon.yxy),
                    GetDist(position - epsilon.yyx)
                );

                return normalize(normal);
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float3 rayOrigin = i.rayOrigin;
                float3 rayDirection = normalize(i.hitPosition - rayOrigin);

                RaymarchResult result = Raymarch(rayOrigin, rayDirection);

                if (_DrawSteps > 0) {
                    return fixed4(
                        (_DrawStepsColor * (result.steps / _MaxSteps)),
                        0
                    );
                }

                if (result.distance > _MaxDistance) {
                    discard;
                }

                float3 position = rayOrigin + rayDirection * result.distance;
                float3 normal = GetNormal(position);

                float3 texColor = tex2D(
                    _MainTex,
                    (position.zx * _MainTex_ST.xy) + _MainTex_ST.zw
                ).rgb;

                // TODO: Doesn't look right
                fixed3 col = AddLighting(position, normal, texColor, _AmbientColor, _Gloss, _SpecularIntensity);
                //return fixed4(col, 0);
                return fixed4(lerp(col, texColor, .8), 0);
            }
            ENDCG
        }
    }
}
