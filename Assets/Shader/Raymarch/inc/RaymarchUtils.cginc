// ===== Lighting =====

fixed3 AddLighting(float3 position, float3 normal, float3 baseColor, float3 ambientColor, float gloss, float specularIntensity) {
    //float3 lightDirection = _WorldSpaceLightPos0; // Won't work with object rotataion
    float3 lightDirection = normalize(mul(unity_WorldToObject, _WorldSpaceLightPos0));
    float3 lightColor     = _LightColor0.rgb;

    // Diffuse
    float lightFalloff        = max(dot(lightDirection, normal), 0);
    float3 directDiffuseLight = lightColor * lightFalloff;

    // Specular
    //float3 cameraPosition = _WorldSpaceCameraPos; // Won't work with object rotataion
    float3 cameraPosition = normalize(mul(unity_WorldToObject, _WorldSpaceCameraPos));
    float3 fragToCamera   = cameraPosition - position;
    float3 viewDirection  = normalize(fragToCamera);

    float3 viewReflect    = reflect(-viewDirection, normal);
    float specularFalloff = max(dot(viewReflect, lightDirection), 0) * specularIntensity;
    specularFalloff       = pow(specularFalloff, gloss);
    float3 directSpecular = specularFalloff * lightColor;

    // Composite
    float3 diffuseLight      = ambientColor + directDiffuseLight;
    float3 finalSurfaceColor = diffuseLight * baseColor + directSpecular;

    return finalSurfaceColor;
}

// ===== Utils =====

float smin(float a, float b, float k = .2) {
    float h = clamp(.5 + .5 * (b-a) / k, 0, 1);
    return lerp(b, a, h) - k * h * (1 - h);
}

float4 sminColor(float a, float b, float3 colA, float3 colB, float k = .2) {
    float h = clamp(.5 + .5 * (b-a) / k, 0, 1);

    return float4(
        lerp(colB,colA,h),
        lerp(b, a, h) - k * h * (1 - h)
    );
}

// ===== Aliases =====

float blend(float a, float b, float k = .2) {
    return smin(a, b, k);
}
