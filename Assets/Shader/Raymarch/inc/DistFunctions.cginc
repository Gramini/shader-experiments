// ===== Distance Functions =====

float DistSphere(float3 position, float radius = .5) {
    return length(position) - radius;
}

float DistTorus(float3 position, float radiusInner, float radiusOuter) {
    return length(float2(length(position.xz) - radiusInner, position.y)) - radiusOuter;
}

float DistBox(float3 position, float3 scale = float3(1, 1, 1)) {
    position = abs(position) - scale * .5;
    return length(max(position, 0)) + min(max(position.x, max(position.y, position.z)), 0);
}

float DistWalnut(float3 position) {
    float torus  = DistTorus(position, .42, .075);
    float sphere = DistSphere(position, .4);

    return smin(torus, sphere, .125);
}

float DistSimpleGyroid (float3 position, float scale = 10., float thickness = .025) {
    position *= scale;
    float distance = dot(sin(position), cos(position.zxy)) / scale;
    return abs(distance) - thickness;
}

float DistGyroid (
    float3 position,
    float scale = 10.,
    float thickness = .025,
    float bias = 1.5
) {
    position *= scale;

    float distance = dot(sin(position), cos(position.zxy)) / scale;
    float rawValue = abs(distance - bias / scale) - thickness;
    return rawValue * (scale * .05);
}




float DistRingExp(float3 position) {
    float distance = 0;
    
    float torus = DistTorus(position, .375, .075);
    float sphere = DistSphere(position, .35);
    float sphere2 = DistSphere(position, .325);
    
    distance = max(torus, sphere);
    distance = max(distance, sphere2);
    
    return distance;
}
