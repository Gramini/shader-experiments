﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Upgrade NOTE: replaced '_Object2World' with '_Object2World'

Shader "Raymarch/Gyroid"
{
    Properties
    {
        [Header(Visuals)]
        [Space]
        _BaseColor("Base Color", Color) = (0.8, 0.8, 0.8, 1)
        _AmbientColor("Ambient Color", Color) = (0.16, 0.16, 0.16, 1)

        [PowerSlider(10.)]
        _Gloss("Gloss", Range(.1, 100.)) = 15.

        [PowerSlider(.5)]
         _SpecularIntensity("Specular Intensity", Range(.1, 1)) = .98

        [PowerSlider(5)]
        _NormalSmoothing("Normal Smoothing", Range(.0001, .1)) = .005

        [Header(Behavior)]
        [Space]
        _MovementDirection("Movement Direction", Vector) = (.1, 1, .1)

        [PowerSlider(2)]
        _MovementSpeed("Movement Speed", Range(0., 1.)) = .05

        [MaterialToggle]
        _WorldSpace ("Use World Space", float) = 0

        [Header(Performance)]
        [Space]
        _MaxSteps ("Max Steps", int) = 100
        _SurfaceDistance ("Surface Distance", float) = .001
        _MaxDistance ("Max Distance", float) = 100

        [Header(Debug)]
        [Space]
        [MaterialToggle]
        _DrawSteps ("Draw Steps", float) = 0
        [HDR]
        _DrawStepsColor ("Draw Step Color", Color) = (1., 0, 0)
    }

    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityLightingCommon.cginc"

            #include "inc/RaymarchUtils.cginc"
            #include "inc/DistFunctions.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float3 rayOrigin : TEXCOORD1;
                float3 hitPosition : TEXCOORD2;
            };

            // Visuals
            float4 _BaseColor;
            float4 _AmbientColor;
            float _Gloss;
            float _SpecularIntensity;
            float _NormalSmoothing;
            float _WorldSpace;

            // Behavior
            float3 _MovementDirection;
            float _MovementSpeed;

            // Performance
            int _MaxSteps;
            float _SurfaceDistance;
            float _MaxDistance;

            // Debug
            float _DrawSteps;
            float3 _DrawStepsColor;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;

                if (_WorldSpace <= 0) {
                    // Object Space
                    o.rayOrigin = mul(unity_WorldToObject, float4(_WorldSpaceCameraPos, 1));
                    o.hitPosition = v.vertex;

                    return o;
                }

                // World Space
                o.rayOrigin = _WorldSpaceCameraPos;
                o.hitPosition = mul(unity_ObjectToWorld, v.vertex);
                return o;
            }

            struct RaymarchResult {
                float distance;
                float steps;
            };

            float GetDist(float3 position) {
                // float3 baseWorldPos = mul(unity_ObjectToWorld, float4(0,0,0,1)).xyz;
                // float3 worldScale = float3(
                //     length(float3(unity_ObjectToWorld[0].x, unity_ObjectToWorld[1].x, unity_ObjectToWorld[2].x)), // scale x axis
                //     length(float3(unity_ObjectToWorld[0].y, unity_ObjectToWorld[1].y, unity_ObjectToWorld[2].y)), // scale y axis
                //     length(float3(unity_ObjectToWorld[0].z, unity_ObjectToWorld[1].z, unity_ObjectToWorld[2].z))  // scale z axis
                // );
                // float box = DistBox(position - baseWorldPos, worldScale);

                float box = DistBox(position) * (1 - _WorldSpace);
                float sphere = DistSphere(position, .6);

                float3 offset = position + (normalize(-_MovementDirection) * _Time.y * _MovementSpeed);

                // float g1 = DistGyroid(offset, 10, .02, 1.3);
                // float g2 = DistGyroid(offset, 20.12, .02, .3);
                // float g3 = DistGyroid(offset, 43.762, .0212, .3312);
                // float g4 = DistGyroid(offset, 78.642, .031, .28534);
                // float g5 = DistGyroid(offset, 123.421, .08412, .62345);
                // float gyroids = g1 + g2 * .075 + g3 * .05 + g4 * .04 + g5 * .025;

                // float gyroid = DistGyroid(offset, 12, .025, .15);
                // float gyroid2 = DistGyroid(offset + float3(5, 5, 5), 10.123, .043, .189263);
                // float gyroid2 = DistGyroid(offset, 34.124, .002213, .0875);
                // float gyroids = gyroid + gyroid2; // max(-gyroid, gyroid2);

                float g1 = DistGyroid(offset, 1.5 * 5.23, .03, 1.4);
                float g2 = DistGyroid(offset, 1.6 * 10.76, .03, .3);
                float g3 = DistGyroid(offset, 1.7 * 20.76, .03, .3);
                float g4 = DistGyroid(offset, 1.8 * 25.76, .03, .3);
                float g5 = DistGyroid(offset, 1.9 * 60.76, .03, .3);
                float gyroids = g1;
                gyroids -= g2 * .12;
                gyroids -= g3 * .08;
                gyroids += g4 * .03;
                gyroids += g5 * .01;
                gyroids *= .8;

                float distance = max(box, gyroids);
                return distance;
            }

            RaymarchResult Raymarch(float3 rayOrigin, float3 rayDirection) {
                float distanceFromOrigin  = 0;
                float distanceFromSurface = 0;

                RaymarchResult result;

                for (int i = 0; i < _MaxSteps; i++) {
                    float3 position = rayOrigin + distanceFromOrigin * rayDirection;
                    distanceFromSurface = GetDist(position);
                    distanceFromOrigin += distanceFromSurface;

                    if (distanceFromSurface < _SurfaceDistance || distanceFromOrigin > _MaxDistance) {
                        result.steps = i;
                        break;
                    }
                }

                result.distance = distanceFromOrigin;

                return result;
            }

            float3 GetNormal(float3 position) {
                float2 epsilon = float2(_NormalSmoothing, 0);

                float3 normal = GetDist(position) -float3(
                    GetDist(position - epsilon.xyy),
                    GetDist(position - epsilon.yxy),
                    GetDist(position - epsilon.yyx)
                );

                return normalize(normal);
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float3 rayOrigin = i.rayOrigin; // float3(0, 0, -3);
                float3 rayDirection = normalize(i.hitPosition - rayOrigin); //normalize(float3(uv.x, uv.y, 1));

                RaymarchResult result = Raymarch(rayOrigin, rayDirection);

                if (_DrawSteps > 0) {
                    return fixed4(
                        (_DrawStepsColor * (result.steps / _MaxSteps)),
                        0
                    );
                }

                if (result.distance > _MaxDistance) {
                    discard;
                }

                float3 position = rayOrigin + rayDirection * result.distance;
                float3 normal = GetNormal(position);

                fixed3 col = AddLighting(position, normal, _BaseColor, _AmbientColor, _Gloss, _SpecularIntensity);
                float3 offset = position + (normalize(-_MovementDirection) * _Time.y * _MovementSpeed);
                float g2 = DistGyroid(offset, 1.6 * 10.76, .03, .3);

                col *= smoothstep(-0.1, .1, g2);

                // return fixed4(col + (normal * .5 + .5), 0);
                return fixed4(col, 0);
            }
            ENDCG
        }
    }
}
