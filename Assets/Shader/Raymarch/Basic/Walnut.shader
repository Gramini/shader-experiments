﻿Shader "Raymarch/Walnut"
{
    Properties
    {
        [Header(Visuals)]
        [Space]
        _BaseColor("Base Color", Color) = (0.8, 0.8, 0.8, 1)
        _AmbientColor("Ambient Color", Color) = (0.16, 0.16, 0.16, 1)
        [PowerSlider(10.)]
        _Gloss("Gloss", Range(.1, 100.)) = 15.
        [PowerSlider(10.)]
        _SpecularIntensity("Specular Intensity", Range(.1, 1)) = .98

        [Header(Performance)]
        [Space]
        _MaxSteps ("Max Steps", int) = 100
        _SurfaceDistance ("Surface Distance", float) = .001
        _MaxDistance ("Max Distance", float) = 100

        [Header(Debug)]
        [Space]
        [MaterialToggle]
        _BlackBox ("BlackBox", float) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityLightingCommon.cginc"
            
            #include "../inc/RaymarchUtils.cginc"
            #include "../inc/DistFunctions.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float3 rayOrigin : TEXCOORD1;
                float3 hitPosition : TEXCOORD2;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                o.rayOrigin = mul(unity_WorldToObject, float4(_WorldSpaceCameraPos, 1));
                o.hitPosition = v.vertex;
                return o;
            }

            // Visuals
            float4 _BaseColor;
            float4 _AmbientColor;
            float _Gloss;
            float _SpecularIntensity;
            
            // Performance
            int _MaxSteps;
            float _SurfaceDistance;
            float _MaxDistance;
           
            // Debug
            float _BlackBox;
            
            float GetDist(float3 position) {
                return DistWalnut(position);
            }
            
            float Raymarch(float3 rayOrigin, float3 rayDirection) {
                float distanceFromOrigin  = 0;
                float distanceFromSurface = 0;

                for (int i = 0; i < _MaxSteps; i++) {
                    float3 position = rayOrigin + distanceFromOrigin * rayDirection;
                    distanceFromSurface = GetDist(position);
                    distanceFromOrigin += distanceFromSurface;

                    if (distanceFromSurface < _SurfaceDistance || distanceFromOrigin > _MaxDistance) {
                        break;
                    }
                }

                return distanceFromOrigin;
            }

            float3 GetNormal(float3 position) {
                float2 epsilon = float2(.01, 0);

                float3 normal = GetDist(position) -float3(
                    GetDist(position - epsilon.xyy),
                    GetDist(position - epsilon.yxy),
                    GetDist(position - epsilon.yyx)
                );

                return normalize(normal);
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float2 uv = i.uv - .5;

                float3 rayOrigin = i.rayOrigin; // float3(0, 0, -3);
                float3 rayDirection = normalize(i.hitPosition - rayOrigin); //normalize(float3(uv.x, uv.y, 1));

                float distance = Raymarch(rayOrigin, rayDirection);

                if (distance > _MaxDistance) {
                    if (_BlackBox <= 0) {
                        discard;
                    }
                    return 0;
                } 

                float3 position = rayOrigin + rayDirection * distance;
                float3 normal = GetNormal(position);
                
                fixed3 col = AddLighting(position, normal, _BaseColor, _AmbientColor, _Gloss, _SpecularIntensity);

                return fixed4(col, 0);
            }
            ENDCG
        }
    }
}
