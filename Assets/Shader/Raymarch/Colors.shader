﻿Shader "Raymarch/Colors"
{
    Properties
    {
        [Header(Visuals)]
        [Space]
        _BaseColor("Base Color", Color) = (0.8, 0.8, 0.8, 1)
        _AmbientColor("Ambient Color", Color) = (0.16, 0.16, 0.16, 1)

        [PowerSlider(10.)]
        _Gloss("Gloss", Range(.1, 100.)) = 15.

        [PowerSlider(.15)]
        _SpecularIntensity("Specular Intensity", Range(.1, 1)) = .98

        [PowerSlider(5)]
        _NormalSmoothing("Normal Smoothing", Range(.000005, .1)) = .005

        [Header(Performance)]
        [Space]
        _MaxSteps ("Max Steps", int) = 100
        _SurfaceDistance ("Surface Distance", float) = .01
        _MaxDistance ("Max Distance", float) = 100

        [Header(Debug)]
        [Space]
        [MaterialToggle]
        _DrawSteps ("Draw Steps", float) = 0
        [HDR]
        _DrawStepsColor ("Draw Step Color", Color) = (1., 0, 0)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityLightingCommon.cginc"

            #include "inc/RaymarchUtils.cginc"
            #include "inc/DistFunctions.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float3 rayOrigin : TEXCOORD1;
                float3 hitPosition : TEXCOORD2;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;

                // Object Space
                o.rayOrigin = mul(unity_WorldToObject, float4(_WorldSpaceCameraPos, 1));
                o.hitPosition = v.vertex;

                // World Space
                // o.rayOrigin = _WorldSpaceCameraPos;
                // o.hitPosition = mul(unity_ObjectToWorld, v.vertex);
                return o;
            }

            struct SdfObject {
                float distance;
                float3 color;
            };

            struct RaymarchResult {
                float distance;
                float steps;
                SdfObject object;
            };

            // TODO: Maybe include in Utils
            SdfObject newSdfObject(float distance, float3 color) {
                SdfObject obj;

                obj.distance = distance;
                obj.color    = color;

                return obj;
            }

            // Visuals
            float4 _BaseColor;
            float4 _AmbientColor;
            float _Gloss;
            float _SpecularIntensity;
            float _NormalSmoothing;

            // Performance
            int _MaxSteps;
            float _SurfaceDistance;
            float _MaxDistance;

            // Debug
            float _DrawSteps;
            float3 _DrawStepsColor;

            // Union (with material data)
            SdfObject minData(SdfObject d1, SdfObject d2)
            {
                if (d1.distance < d2.distance) {
                    return d1;
                }

                return d2;
            }

            SdfObject Blend(SdfObject d1, SdfObject d2, float k = 0.2)
            {
                float4 blended = sminColor(d1.distance, d2.distance, d1.color, d2.color, k);
                return newSdfObject(
                    blended.w,
                    blended.xyz
                );
            }

            // Union (with material data) and not smooth color
            SdfObject sminData(SdfObject d1, SdfObject d2, float k = .2)
            {
                return newSdfObject(
                    smin(d1.distance, d2.distance, k),
                    minData(d1, d2).color
                );
            }

            SdfObject GetDist(float3 position) {
                float3 offset = float3(0.25, 0., 0.);

                SdfObject d1 = newSdfObject(DistSphere(position + offset, .2), float3(.4, .0, .0));
                SdfObject d2 = newSdfObject(DistSphere(position - offset, .2), float3(.0, .0, .6));
                SdfObject d3 = newSdfObject(DistSphere(position - float3(0, .4, 0), .2), float3(.0, .5, .0));
                SdfObject d4 = newSdfObject(DistBox(position - float3(0., -.25, .0), float3(1, .1, .75)), float3(.7, .9, .6));

                SdfObject result = Blend(d1, d2, .2);
                result = Blend(result, d3, .2);
                result = Blend(result, d4, .1);

                return result;
            }

            RaymarchResult Raymarch(float3 rayOrigin, float3 rayDirection) {
                float distanceFromOrigin  = 0;
                float distanceFromSurface = 0;

                RaymarchResult result;

                for (int i = 0; i < _MaxSteps; i++) {
                    float3 position = rayOrigin + distanceFromOrigin * rayDirection;
                    SdfObject obj = GetDist(position);

                    distanceFromSurface = obj.distance;
                    distanceFromOrigin += distanceFromSurface;

                    if (distanceFromSurface < _SurfaceDistance || distanceFromOrigin > _MaxDistance) {
                        result.steps  = i;
                        result.object = obj;

                        break;
                    }
                }

                result.distance = distanceFromOrigin;

                return result;
            }

            float3 GetNormal(float3 position) {
                float2 epsilon = float2(_NormalSmoothing, 0);

                float3 normal = GetDist(position).distance -float3(
                    GetDist(position - epsilon.xyy).distance,
                    GetDist(position - epsilon.yxy).distance,
                    GetDist(position - epsilon.yyx).distance
                );

                return normalize(normal);
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float2 uv = i.uv - .5;

                float3 rayOrigin = i.rayOrigin; // float3(0, 0, -3);
                float3 rayDirection = normalize(i.hitPosition - rayOrigin); //normalize(float3(uv.x, uv.y, 1));

                RaymarchResult result = Raymarch(rayOrigin, rayDirection);

                if (_DrawSteps > 0) {
                    return fixed4(
                        (_DrawStepsColor * (result.steps / _MaxSteps)),
                        0
                    );
                }

                if (result.distance > _MaxDistance) {
                    discard;
                }

                float3 position = rayOrigin + rayDirection * result.distance;
                float3 normal = GetNormal(position);

                fixed3 col = AddLighting(position, normal, result.object.color, _AmbientColor, _Gloss, _SpecularIntensity);

                return fixed4(col, 0);
            }
            ENDCG
        }
    }
}
