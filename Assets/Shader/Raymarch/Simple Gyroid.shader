﻿Shader "Raymarch/Simple Gyroid"
{
    Properties
    {
        [Header(Visuals)]
        [Space]
        _BaseColor("Base Color", Color) = (0.8, 0.8, 0.8, 1)
        _AmbientColor("Ambient Color", Color) = (0.16, 0.16, 0.16, 1)

        [PowerSlider(10.)]
        _Gloss("Gloss", Range(.1, 100.)) = 15.

        [PowerSlider(.15)]
         _SpecularIntensity("Specular Intensity", Range(.1, 1)) = .98

        [PowerSlider(5)]
        _NormalSmoothing("Normal Smoothing", Range(.000005, .1)) = .005

        [Header(Behavior)]
        [Space]
        _MovementDirection("Movement Direction", Vector) = (.1, 1, .1)

        [PowerSlider(2)]
        _MovementSpeed("Movement Speed", Range(0., 1.)) = .05

        [Header(Performance)]
        [Space]
        _MaxSteps ("Max Steps", int) = 100
        _SurfaceDistance ("Surface Distance", float) = .001
        _MaxDistance ("Max Distance", float) = 100

        [Header(Debug)]
        [Space]
        [MaterialToggle]
        _DrawSteps ("Draw Steps", float) = 0
        [HDR]
        _DrawStepsColor ("Draw Step Color", Color) = (1., 0, 0)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityLightingCommon.cginc"

            #include "inc/RaymarchUtils.cginc"
            #include "inc/DistFunctions.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float3 rayOrigin : TEXCOORD1;
                float3 hitPosition : TEXCOORD2;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                o.rayOrigin = mul(unity_WorldToObject, float4(_WorldSpaceCameraPos, 1));
                o.hitPosition = v.vertex;
                return o;
            }

            struct RaymarchResult {
                float distance;
                float steps;
            };

            // Visuals
            float4 _BaseColor;
            float4 _AmbientColor;
            float _Gloss;
            float _SpecularIntensity;
            float _NormalSmoothing;

            // Behavior
            float3 _MovementDirection;
            float _MovementSpeed;

            // Performance
            int _MaxSteps;
            float _SurfaceDistance;
            float _MaxDistance;

            // Debug
            float _DrawSteps;
            float3 _DrawStepsColor;

            float GetDist(float3 position) {
                float scale = 10;

                float box = DistBox(position);
                float sphere = DistSphere(position, .6);

                float3 offset = position + (normalize(-_MovementDirection) * _Time.y * _MovementSpeed);
                float gyroid = DistSimpleGyroid(offset, 15);

                float distance = max(max(box, sphere), gyroid);
                return distance;
            }

            RaymarchResult Raymarch(float3 rayOrigin, float3 rayDirection) {
                float distanceFromOrigin  = 0;
                float distanceFromSurface = 0;

                RaymarchResult result;

                for (int i = 0; i < _MaxSteps; i++) {
                    float3 position = rayOrigin + distanceFromOrigin * rayDirection;
                    distanceFromSurface = GetDist(position);
                    distanceFromOrigin += distanceFromSurface;

                    if (distanceFromSurface < _SurfaceDistance || distanceFromOrigin > _MaxDistance) {
                        result.steps = i;
                        break;
                    }
                }

                result.distance = distanceFromOrigin;

                return result;
            }

            float3 GetNormal(float3 position) {
                float2 epsilon = float2(_NormalSmoothing, 0);

                float3 normal = GetDist(position) -float3(
                    GetDist(position - epsilon.xyy),
                    GetDist(position - epsilon.yxy),
                    GetDist(position - epsilon.yyx)
                );

                return normalize(normal);
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float3 rayOrigin = i.rayOrigin; // float3(0, 0, -3);
                float3 rayDirection = normalize(i.hitPosition - rayOrigin); //normalize(float3(uv.x, uv.y, 1));

                RaymarchResult result = Raymarch(rayOrigin, rayDirection);

                if (_DrawSteps > 0) {
                    return fixed4(
                        (_DrawStepsColor * (result.steps / _MaxSteps)),
                        0
                    );
                }

                if (result.distance > _MaxDistance) {
                    discard;
                }

                float3 position = rayOrigin + rayDirection * result.distance;
                float3 normal = GetNormal(position);

                fixed3 col = AddLighting(position, normal, _BaseColor, _AmbientColor, _Gloss, _SpecularIntensity);

                return fixed4(col, 0);
            }
            ENDCG
        }
    }
}
