﻿Shader "Unlit/Wireframe"
{
    Properties
    {
        [HDR]
        _WireColor ("Wire Color", Color) = (0.61, 0.19, 0.12, 1)
        
        [PowerSlider(10.)]
        _Threshold ("Threshold", Range(.0001, 1.)) = .99
    }
    
    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float3 normal : TEXCOORD1;
            };

            v2f vert (appdata v)
            {
                v2f o;

                o.normal = v.normal;
                o.vertex = UnityObjectToClipPos(v.vertex);
                // o.uv = TRANSFORM_TEX(v.uv, _MainTex);

                return o;
            }

            float4 _WireColor;
            float _Threshold;

            fixed4 frag (v2f i) : SV_Target
            {
                float3 normal = i.normal;
                float len     = length(normal);
                
                float powered = pow(len, 1000);
            
                if (powered < _Threshold) {
                    discard;
                }
                
                return _WireColor;
                // return i.;
            }
            ENDCG
        }
    }
}
