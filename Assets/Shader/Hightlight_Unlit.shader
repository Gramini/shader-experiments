﻿Shader "Unlit/Highlight"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}

        //[Header(Highlight)]
        //[Space]
        //_Position("Center", Vector) = (0, 0, 0, 0)
        //_Radius("Radius", Range(0, 100)) = 1
        //_Softness("Softness", Range(0, 100)) = .1
    }

    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float3 worldPos : TEXCOORD1;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            uniform int _FA_Highlight_Count = 0;
            uniform float3 _FA_Highlight_Positions[10];
            uniform float _FA_Highlight_Radii[10];
            uniform float _FA_Highlight_Softnesses[10];

            v2f vert (appdata v)
            {
                v2f o;

                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.worldPos = mul(unity_ObjectToWorld, v.vertex);

                return o;
            }

            fixed4 frag (v2f input) : SV_Target
            {
                fixed4 texCol       = tex2D(_MainTex, input.uv);
                fixed greyscale     = Luminance(texCol);
                fixed4 greyscaleCol = fixed4(greyscale, greyscale, greyscale, 1);
                fixed4 lerpColor    = greyscaleCol;

                [loop]
                for (int i = 0; i < _FA_Highlight_Count; i++) {
                    half d = distance(_FA_Highlight_Positions[i], input.worldPos);
                    half sum = saturate((d - _FA_Highlight_Radii[i]) / -_FA_Highlight_Softnesses[i]);

                    lerpColor = max(lerp(greyscaleCol, texCol, sum), lerpColor);
                }

                return lerpColor;
            }
            ENDCG
        }
    }
}
